# id-wall-desafio

- Para executar o projeto, crie antes um arquivo: terraform.tfvars contendo sua ACCESS KEY e SECRET KEY, i.e:

cat terraform.tfvars
access_key = "sua_akey"
secret_key = "sua_skey"


- Após isso, basta executar:

terraform apply

Será pedido: a zona desejada e o range de IP a ser liberado pelo Security Group
