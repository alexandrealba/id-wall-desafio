#!/bin/bash


apt-get remove docker docker-engine docker.io -y

apt-get install apt-transport-https ca-certificates curl software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt-get update


apt-get install docker-ce -y


docker pull httpd

docker run -dit --name id_wall -p 80:80 httpd
