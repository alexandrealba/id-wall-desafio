### Provider And Access Definitions
provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}


### Creating Key Pair

 resource "aws_key_pair" "id_wall_key" {
  key_name = "id_wall_key"
  public_key = "${file("${var.key_path}")}"
}



### Creating the single-instance
resource "aws_instance" "mark1" {
  ami           = "ami-0318cb6e2f90d688b"
  instance_type = "t2.micro"
  vpc_security_group_ids = ["${aws_security_group.development_http_https.id}","${aws_security_group.development_allow_ssh.id}","${aws_security_group.allow_connection_
to_the_world.id}"]
  subnet_id = "${aws_subnet.public-subnet.id}"
  key_name = "${aws_key_pair.id_wall_key.id}"
  user_data = "${file("install.sh")}"

}


### Creating VPC

resource "aws_vpc" "id_wall" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags {
    Name = "id_wall"
  }
}


resource "aws_subnet" "public-subnet" {
  vpc_id = "${aws_vpc.id_wall.id}"
  cidr_block = "${var.public_subnet_cidr}"
  availability_zone = "sa-east-1a"

  tags {
    Name = "Web Public Subnet"
  }
}

# Define the private subnet
resource "aws_subnet" "private-subnet" {
  vpc_id = "${aws_vpc.id_wall.id}"
  cidr_block = "${var.private_subnet_cidr}"
  availability_zone = "sa-east-1a"

  tags {
    Name = "Database Private Subnet"
  }
}


# Define the internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.id_wall.id}"

  tags {
    Name = "VPC IGW"
  }
}




resource "aws_route_table" "web-public-rt" {
  vpc_id = "${aws_vpc.id_wall.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags {
    Name = "Public Subnet RT"
  }
}

# Assign the route table to the public Subnet
resource "aws_route_table_association" "web-public-rt" {
  subnet_id = "${aws_subnet.public-subnet.id}"
  route_table_id = "${aws_route_table.web-public-rt.id}"
}




### Assigning Elastic IP

resource "aws_eip" "ip" {
  instance = "${aws_instance.mark1.id}"
}


### Defining the output var to show the public IP of the instance

output "ip" {
  value = "${aws_eip.ip.public_ip}"
}


resource "aws_security_group" "allow_connection_to_the_world" {
name = "allow_connection_to_the_world"
description = "allow_connection_to_the_world"
vpc_id = "${aws_vpc.id_wall.id}"

egress {
from_port = 0
to_port = 0
protocol = "-1"
cidr_blocks = ["0.0.0.0/0"]
}


tags {
Name = "allow_connection_to_the_world"
}
}


resource "aws_security_group" "development_http_https" {
name = "development_http_https"
description = "Allow inbound HTTP and HTTPS traffic"
vpc_id = "${aws_vpc.id_wall.id}"

ingress {
from_port = 80
to_port = 80
protocol = "tcp"
cidr_blocks = ["0.0.0.0/0"]
}

ingress {
from_port = 443
to_port = 443
protocol = "tcp"
cidr_blocks = ["0.0.0.0/0"]
}


tags {
Name = "Allow inbound HTTP and HTTPS traffic"
}
}


resource "aws_security_group" "development_allow_ssh" {
name = "development_allow_ssh"
description = "Allow inbound SSH traffic from a specific IP range"
vpc_id = "${aws_vpc.id_wall.id}"

ingress {
from_port = 22
to_port = 22
protocol = "tcp"
cidr_blocks = ["${var.ssh_allowed_range}"]
}


tags {
Name = "Allow inbound SSH traffic from a specific IP range"
}
}

