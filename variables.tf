variable "access_key" {}
variable "secret_key" {}
variable "region"     {}
variable "ssh_allowed_range" {}
variable "key_path" { default = "~/.ssh/id_rsa.pub" }
variable "vpc_cidr" { default = "10.0.0.0/16" }
variable "public_subnet_cidr" {default = "10.0.1.0/24"}
variable "private_subnet_cidr" { default = "10.0.2.0/24"}
